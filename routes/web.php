<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	return view('welcome');
});

Route::get('/home','Usercontroller@home');
Route::get('/properties','Usercontroller@properties');
Route::get('/register','Usercontroller@register');
Route::get('/login','Usercontroller@login');
Route::get('/propertiesdetail','Usercontroller@propertiesdetail');
Route::get('/propertiesregister','Usercontroller@propertiesregister');
Route::get('/logout','Usercontroller@logout');

Route::post('/checklogin','Usercontroller@checklogin');

Route::post('/checkregister','Usercontroller@checkregister');



Route::get('/admin','AdminController@homeadmin');
Route::get('/customer','AdminController@customer');
Route::get('/customerhotel','AdminController@customerhotel');
Route::get('/hotel','AdminController@hotel');
Route::post('/updateData','AdminController@updateData');

Route::get('/delete_data/{customer_id}', 'AdminController@deleteData');
