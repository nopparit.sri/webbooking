<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class AdminController extends Controller
{
    public function homeadmin(){

    	return view ('/admin/index');
    }

    public function customer(){
    	$data = DB::table('customer')->where('customer_status','customer')->get();
    	return view ('/admin/table',['data' => $data]);
    }

     public function customerhotel(){
    	$data = DB::table('customer')->where('customer_status','owner')->get();
    	return view ('/admin/tablecus',['data' => $data]);
    }

    public function hotel(){
    	$data = DB::table('hotel')->get();
    	return view ('/admin/hotel',['data' => $data]);
    }
    public function deleteData($id){
    	DB::table('customer')->where('customer_id',$id)->delete();
    	return redirect('/admin');
    }
    public function updateData(Request $request){
    	$data = $request->all();
    	$id = $data['id'];
    	$name = $data['text-input-name'];
    	$tel = $data['text-input-tel'];
    	$address = $data['text-input-address'];
    	$mail = $data['text-input-mail'];
    	DB::table('customer')->where('customer_id',$id)->update([
    		'customer_name' => $name,
    		'customer_address' => $address,
    		'customer_tel' => $tel,
    		'customer_email' => $mail
    	]);
    	echo json_encode($data);
    }
}
