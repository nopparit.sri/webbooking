<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class Usercontroller extends Controller
{
    //
    public function home(){
    	return view ('/home');
    }
    public function properties(){
    	return view ('/properties');
    }
    public function register(){
    	return view ('/register');
    }
    public function login(){
    	return view ('/login');
    }
    public function propertiesdetail(){
    	return view ('/propertiesdetail');
    }
    public function propertiesregister(){
        return view ('/propertiesregister');
    }

    public function checklogin(Request $request){
        session_start();
        $data = $request->all();
        $user = $data['user'];
        $pass = $data['pass'];
        $status = $data['status'];
        $d = array(
            'customer_name' => $user,
            'customer_password' => $pass
        );

        $ans = DB::table('customer')->where($d)->get();
        $row_count = count($ans);

        if($row_count == 1 && $status == 'customer'){
            session([
                'user' => $ans[0]->customer_name,
                'pass' => $ans[0]->customer_password,
            ]);
            $_SESSION['user']=$ans[0]->customer_name;
            return redirect('/home');  
        }

        if($row_count == 1 && $status == 'owner'){
            session([
                'user' => $ans[0]->customer_name,
                'pass' => $ans[0]->customer_password,
            ]);
            return redirect('/propertiesregister');  
        }

        if($row_count == 1 && $status == 'admin'){
            session([
                'user' => $ans[0]->customer_name,
                'pass' => $ans[0]->customer_password,
            ]);
            return redirect('/admin');  
        }
    }
    public function logout(){
        session_start();
        session_destroy();
        return redirect('login');
    }

    public function checkregister(Request $request){
     $data = $request->all();
     DB::table('customer')->insert([
        'customer_name' => $data['user'],
        'customer_password' => $data['pass'],
        'customer_address' => $data['address'],
        'customer_tel' => $data['tel'],
        'customer_email' => $data['email'],
        'customer_status' => $data['status'],

    ]);
     return redirect('login');
 }




}
