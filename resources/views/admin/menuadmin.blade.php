<header class="header-mobile d-block d-lg-none">
    <div class="header-mobile__bar">
        <div class="container-fluid">
            <div class="header-mobile-inner">
                <a class="logo" href="index.html">
                    <img src="images/icon/logo.png" alt="CoolAdmin" />
                </a>
                <button class="hamburger hamburger--slider" type="button">
                    <span class="hamburger-box">
                        <span class="hamburger-inner"></span>
                    </span>
                </button>
            </div>
        </div>
    </div>
    <nav class="navbar-mobile">
        <div class="container-fluid">
            <ul class="navbar-mobile__list list-unstyled">
                <li class="has-sub">
                    <a class="js-arrow" href="#">
                        <i class="fas fa-tachometer-alt"></i>Dashboard</a>
                    </li>
                    <li>
                        <a href="chart.html">
                            <i class="fas fa-chart-bar"></i>Charts</a>
                        </li>
                        <li>
                            <a href="table.html">
                                <i class="fas fa-table"></i>Tables</a>
                            </li>
                            <li>
                                <a href="form.html">
                                    <i class="far fa-check-square"></i>Forms</a>
                                </li>

                            </ul>
                        </div>
                    </nav>
</header>
                <aside class="menu-sidebar d-none d-lg-block">
                    <div class="logo">
                        <a href="#">
                            <img src="images/icon/logo.png" alt="Cool Admin" />
                        </a>
                    </div>
                    <div class="menu-sidebar__content js-scrollbar1">
                        <nav class="navbar-sidebar">
                            <ul class="list-unstyled navbar__list">
                                <li class="active has-sub">
                                    <a class="js-arrow" href="/admin">
                                        <i class="fas fa-tachometer-alt"></i>Genaral</a>

                                    </li>
                                    <li>
                                        <a href="/customer">
                                            <i class="fas fa-chart-bar"></i>Customer</a>
                                        </li>
                                        <li>
                                            <a href="/customerhotel">
                                                <i class="fas fa-table"></i>CustomerHotel</a>
                                            </li>
                                            <li>
                                                <a href="/hotel">
                                                    <i class="far fa-check-square"></i>Hotel</a>
                                                </li>
                                                


                                            </ul>
                                        </nav>
                                    </div>
                                </aside>