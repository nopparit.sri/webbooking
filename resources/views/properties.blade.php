<!DOCTYPE html>
<html>
<head>

	<!-- Basic -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">	

	<title>MIC & OH - Easy Booking for You</title>	

	<meta name="keywords" content="HTML5 Template" />
	<meta name="description" content="Porto - Responsive HTML5 Template">
	<meta name="author" content="okler.net">

	<!-- Favicon -->
	<link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon" />
	<link rel="apple-touch-icon" href="img/apple-touch-icon.png">

	<!-- Mobile Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">

	<!-- Web Fonts  -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">

	<!-- Vendor CSS -->
	<link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="vendor/font-awesome/css/fontawesome-all.min.css">
	<link rel="stylesheet" href="vendor/animate/animate.min.css">
	<link rel="stylesheet" href="vendor/simple-line-icons/css/simple-line-icons.min.css">
	<link rel="stylesheet" href="vendor/owl.carousel/assets/owl.carousel.min.css">
	<link rel="stylesheet" href="vendor/owl.carousel/assets/owl.theme.default.min.css">
	<link rel="stylesheet" href="vendor/magnific-popup/magnific-popup.min.css">

	<!-- Theme CSS -->
	<link rel="stylesheet" href="css/theme.css">
	<link rel="stylesheet" href="css/theme-elements.css">
	<link rel="stylesheet" href="css/theme-blog.css">
	<link rel="stylesheet" href="css/theme-shop.css">

	<!-- Current Page CSS -->
	<link rel="stylesheet" href="vendor/rs-plugin/css/settings.css">
	<link rel="stylesheet" href="vendor/rs-plugin/css/layers.css">
	<link rel="stylesheet" href="vendor/rs-plugin/css/navigation.css">

	<!-- Demo CSS -->
	<link rel="stylesheet" href="css/demos/demo-real-estate.css">

	<!-- Skin CSS -->
	<link rel="stylesheet" href="css/skins/skin-real-estate.css"> 

	<!-- Theme Custom CSS -->
	<link rel="stylesheet" href="css/custom.css">

	<!-- Head Libs -->
	<script src="vendor/modernizr/modernizr.min.js"></script>

</head>
<body class="loading-overlay-showing" data-loading-overlay>
	<div class="loading-overlay">
		<div class="bounce-loader">
			<div class="bounce1"></div>
			<div class="bounce2"></div>
			<div class="bounce3"></div>
		</div>
	</div>

	<div class="body">
		<header id="header" data-plugin-options="{'stickyEnabled': true, 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': true, 'stickyStartAt': 37, 'stickySetTop': '-37px', 'stickyChangeLogo': false}">
			<div class="header-body background-color-primary pt-0 pb-0">

				<div class="header-container container custom-position-initial">
					<div class="header-row">
						<div class="header-column">
							<div class="header-row">
								<div class="header-logo">
									<a href="/home">
										<img alt="Porto" width="143" height="40" src="img/demos/real-estate/logo-real-estate-new.png">
									</a>
								</div>
							</div>
						</div>
						<div class="header-column justify-content-end">
							<div class="header-row">
								<div class="header-nav header-nav-stripe">
									<div class="header-nav-main header-nav-main-effect-1 header-nav-main-sub-effect-1 m-0">
										<nav class="collapse">
											<ul class="nav nav-pills" id="mainNav">
												

												<li class="dropdown-full-color dropdown-quaternary">
													<a class="nav-link active" href="/properties">
														List Your Propertry
													</a>
												</li>

												<li class="dropdown dropdown-full-color dropdown-quaternary">
													<a class="nav-link" href="/register">
														Register
													</a>

												</li>
												<li class="dropdown-full-color dropdown-quaternary">
													<a class="nav-link" href="/login">
														Log-in
													</a>
												</li>
												
												
											</ul>
										</nav>
									</div>
									<button class="btn header-btn-collapse-nav" data-toggle="collapse" data-target=".header-nav-main nav">
										<i class="fas fa-bars"></i>
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</header>
		<div role="main" class="main">

			<section class="page-header page-header-light page-header-more-padding">
				<div class="container">
					<div class="row align-items-center">
						<div class="col-lg-6">
							<h1>List Your Apartment , Hotel</h1>
						</div>
						<div class="col-lg-6">
							<ul class="breadcrumb">
								<li><a href="/home">Home</a></li>
								<li class="active">List Your Propertry</li>
							</ul>
						</div>
					</div>

				</div>
			</form>
		</div>
	</div>
</div>
</section>
<div class="container">

	<div class="row">
		<div class="col-lg-9">

			<form class="" action="/checkregister" method="POST">
				{{ csrf_field() }}
				<div class="form-row">
					<div class="form-group col-md-6">
						<label for="firstname">Username</label>
						<input type="text" class="form-control" placeholder="Username" name="user" value="hh">
					</div>
					<div class="form-group col-md-6">
						<label for="firstname">Password</label>
						<input type="password" class="form-control" placeholder="Password" name="pass" value="hh">
					</div>

					<div class="form-group col-md-6">
						<label for="inputTel4">Tel</label>
						<input type="Tel" class="form-control" id="inputTel4" placeholder="Tel" name="tel" value="hh">
					</div>
					<div class="form-group col-md-6">
						<label for="inputPassword4">E-mail</label>
						<input type="text" class="form-control" id="inputPassword4" placeholder="E-mail" name="email" value="hh">
					</div>
					<div class="form-group col-md-6">
						<label for="inputAddress">Address</label>
						<input type="text" class="form-control" id="inputAddress" placeholder="1234 Main St" name="address" value="h">
					</div>

					<div class="form-group col-md-6">
						<label for="exampleFormControlSelect1">Status</label>
						<select class="form-control" id="exampleFormControlSelect1" name="status">
							
							<option>owner</option>
							
						</select>
					</div>
					<div class="form-group" >
						<button type="submit" class="btn btn-primary">Create</button>

					</div>
				</div>
			</form>
		</div>
	</div>



</form>

</div>
</div>

</aside>
</div>
</div>
</div>

<footer id="footer" class="m-0 custom-background-color-1">
	<div class="container">
		<div class="row">
			<div class="col-lg-3">
				<h4 class="mb-3">Porto Real Estate</h4>
				<p class="custom-color-2 mb-0">
					123 Porto Blvd, Suite 100<br>
					New York, NY<br>
					Phone : 123-456-7890<br>
					Email : <a class="text-color-secondary" href="mailto:mail@example.com">mail@example.com</a>
				</p>
			</div>
			<div class="col-lg-2">
				<h4 class="mb-3">Properties</h4>
				<nav class="nav-footer">
					<ul class="custom-list-style-1 mb-0">
						<li>
							<a href="demo-real-estate-properties.html" class="custom-color-2 text-decoration-none">
								For Sale
							</a>
						</li>
						<li>
							<a href="demo-real-estate-properties.html" class="custom-color-2 text-decoration-none">
								For Rent
							</a>
						</li>
					</ul>
				</nav>
			</div>
			<div class="col-lg-2">
				<h4 class="mb-3">Links</h4>
				<nav class="nav-footer">
					<ul class="custom-list-style-1 mb-0">
						<li>
							<a href="demo-real-estate-agents.html" class="custom-color-2 text-decoration-none">
								Agents
							</a>
						</li>
						<li>
							<a href="demo-real-estate-who-we-are.html" class="custom-color-2 text-decoration-none">
								Who We Are
							</a>
						</li>
						<li>
							<a href="demo-real-estate-contact.html" class="custom-color-2 text-decoration-none">
								Contact
							</a>
						</li>
					</ul>
				</nav>
			</div>
			<div class="col-lg-5">
				<h4 class="mb-3">Latest Tweet</h4>
				<div id="tweet" class="twitter" data-plugin-tweets data-plugin-options="{'username': '', 'count': 1}">
					<p>Please wait...</p>
				</div>
			</div>
		</div>
	</div>
	<div class="footer-copyright custom-background-color-1 pb-0">
		<div class="container">
			<div class="row pt-3 pb-3">
				<div class="col-lg-12 left m-0">
					<p>© Copyright 2018. All Rights Reserved.</p>
				</div>
			</div>
		</div>
	</div>
</footer>
</div>
</div>

<!-- Vendor -->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/jquery.appear/jquery.appear.min.js"></script>
<script src="vendor/jquery.easing/jquery.easing.min.js"></script>
<script src="vendor/jquery-cookie/jquery-cookie.min.js"></script>
<script src="vendor/popper/umd/popper.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="vendor/common/common.min.js"></script>
<script src="vendor/jquery.validation/jquery.validation.min.js"></script>
<script src="vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
<script src="vendor/jquery.gmap/jquery.gmap.min.js"></script>
<script src="vendor/jquery.lazyload/jquery.lazyload.min.js"></script>
<script src="vendor/isotope/jquery.isotope.min.js"></script>
<script src="vendor/owl.carousel/owl.carousel.min.js"></script>
<script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
<script src="vendor/vide/vide.min.js"></script>

<!-- Theme Base, Components and Settings -->
<script src="js/theme.js"></script>

<!-- Current Page Vendor and Views -->
<script src="vendor/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
<script src="vendor/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

<!-- Current Page Vendor and Views -->
<script src="js/views/view.contact.js"></script>

<!-- Demo -->
<script src="js/demos/demo-real-estate.js"></script>

<!-- Theme Custom -->
<script src="js/custom.js"></script>

<!-- Theme Initialization Files -->
<script src="js/theme.init.js"></script>




		<!-- Google Analytics: Change UA-XXXXX-X to be your site's ID. Go to http://www.google.com/analytics/ for more information.
		<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		
			ga('create', 'UA-12345678-1', 'auto');
			ga('send', 'pageview');
		</script>
	-->


</body>
</html>
